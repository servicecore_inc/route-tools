<?php

namespace ServiceCore\RouteTools;

use ServiceCore\RouteTools\Context\Factory\GuidanceHere as GuidanceHereFactory;
use ServiceCore\RouteTools\Context\Factory\MatrixRoutingHere as MatrixRoutingHereFactory;
use ServiceCore\RouteTools\Context\Factory\OptimizeHere as OptimizeHereFactory;
use ServiceCore\RouteTools\Context\Factory\OptimizeRoute4Me as OptimizeRoute4MeFactory;
use ServiceCore\RouteTools\Context\GuidanceHere;
use ServiceCore\RouteTools\Context\MatrixRoutingHere;
use ServiceCore\RouteTools\Context\OptimizeHere;
use ServiceCore\RouteTools\Context\OptimizeRoute4Me;
use ServiceCore\RouteTools\Service\Factory\RouteTools as RouteToolsFactory;
use ServiceCore\RouteTools\Service\RouteTools as RouteToolsService;

return [
    'route-tools' => [
        'providers' => [
            RouteToolsService::ROUTE4ME => [
                'apiKey' => null
            ],
            RouteToolsService::HERE     => [
                'apiKey' => null
            ],
        ]
    ],

    'service_manager' => [
        'factories' => [
            RouteToolsService::class => RouteToolsFactory::class,
            OptimizeRoute4Me::class  => OptimizeRoute4MeFactory::class,
            OptimizeHere::class      => OptimizeHereFactory::class,
            GuidanceHere::class      => GuidanceHereFactory::class,
            MatrixRoutingHere::class => MatrixRoutingHereFactory::class
        ]
    ]
];
