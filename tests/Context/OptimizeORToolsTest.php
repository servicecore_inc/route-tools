<?php

namespace ServiceCore\RouteTools\Test\Context;

use PHPUnit\Framework\TestCase;
use ServiceCore\RouteTools\Collection\Waypoint as WaypointCollection;
use ServiceCore\RouteTools\Context\OptimizeORTools;
use ServiceCore\RouteTools\Data\Waypoint;

// This test has been disabled as ORTools v6 is no longer available
// and our current solution is not compatible with v7+
class OptimizeORToolsTest extends TestCase
{
//    public function testOptimizeSendsOptimizedResults(): void
//    {
//        $optimizeContext = new OptimizeORTools();
//
//        $waypoints = new WaypointCollection();
//
//        $waypoints->add(1, new Waypoint(1, 1, 1, null, null, null));
//        $waypoints->add(2, new Waypoint(2, 10, 10, null, null, null));
//        $waypoints->add(3, new Waypoint(3, 5, 5, null, null, null));
//
//        $results = $optimizeContext($waypoints);
//
//        $expectedIds = [1, 3, 2];
//
//        foreach ($results->get() as $key => $result) {
//            $expected = \array_shift($expectedIds);
//            self::assertEquals($expected, $key);
//            self::assertEquals($waypoints->get($expected), $result);
//        }
//    }
}
