<?php

namespace ServiceCore\RouteTools\Test\Adapter;

use DateTime;
use PHPUnit\Framework\TestCase;
use ServiceCore\RouteTools\Adapter\Here;
use ServiceCore\RouteTools\Collection\Waypoint as WaypointCollection;
use ServiceCore\RouteTools\Data\Waypoint;
use ServiceCore\RouteTools\Data\OptimizableInterface;
use ServiceCore\RouteTools\Exception\InvalidArgumentException;
use ServiceCore\RouteTools\Exception\OptimizationFailed;

class HereTest extends TestCase
{
    public function testOptimizeSequence(): void
    {
        $adapter = new Here('');

        $waypoints = new WaypointCollection([
            $this->generateCoordinates(0),
            $this->generateCoordinates(1),
            $this->generateCoordinates(2),
            $this->generateCoordinates(3),
        ]);

        $waypoints->setDeparture(new DateTime());

        $this->expectException(OptimizationFailed::class);
        $adapter->optimizeSequence($waypoints);
    }

    public function testGuidanceSequence(): void
    {
        $adapter = new Here('');

        $waypoints = new WaypointCollection([
            $this->generateCoordinates(0),
            $this->generateCoordinates(1),
            $this->generateCoordinates(2),
            $this->generateCoordinates(3),
        ]);

        $this->expectException(OptimizationFailed::class);
        $adapter->guidanceSequence($waypoints);
    }

    public function testFindDistanceBetweenPoints(): void
    {
        $adapter = new Here('');

        $origin = new Waypoint(
            1,
            'origin',
            1,
            1,
            null,
            null,
            null
        );

        $destinations = new WaypointCollection([
            new Waypoint(
                1,
                'destination1',
                1,
                1,
                null,
                null,
                null
            )
        ]);

        $this->expectException(OptimizationFailed::class);
        $adapter->findDistanceBetweenPoints(
            $origin,
            $destinations
        );
    }

    public function testOptimizeThrowsExceptionWithNullCoordinates(): void
    {
        $adapter = new Here('');

        $waypoints = new WaypointCollection([
            $this->generateCoordinates(0),
            $this->generateCoordinates(1),
            $this->generateCoordinates(2),
            $this->generateInvalidCoordinates(3),
        ]);

        $waypoints->setDeparture(new DateTime());

        $this->expectException(InvalidArgumentException::class);
        $adapter->optimizeSequence($waypoints);
    }

    public function testOptimizeThrowsExceptionWithNullDeparture(): void
    {
        $adapter = new Here('');

        $waypoints = new WaypointCollection([
            $this->generateCoordinates(0),
            $this->generateCoordinates(1),
            $this->generateCoordinates(2),
            $this->generateInvalidCoordinates(3),
        ]);

        $this->expectException(InvalidArgumentException::class);
        $adapter->optimizeSequence($waypoints);
    }

    public function testGuidanceThrowsExceptionWithNullCoordinates(): void
    {
        $adapter = new Here('');

        $waypoints = new WaypointCollection([
            $this->generateCoordinates(0),
            $this->generateCoordinates(1),
            $this->generateCoordinates(2),
            $this->generateInvalidCoordinates(3),
        ]);

        $this->expectException(InvalidArgumentException::class);
        $adapter->guidanceSequence($waypoints);
    }

    private function generateCoordinates(int $id): OptimizableInterface
    {
        return new Waypoint(
            $id,
            "J{$id}",
            (\random_int(5000, 10000) / 10000) + 39,
            (\random_int(5000, 10000) / 10000) - 105,
            null,
            null,
            null
        );
    }

    private function generateInvalidCoordinates(int $id): OptimizableInterface
    {
        return new Waypoint(
            $id,
            "J{$id}",
            null,
            null,
            null,
            null,
            null
        );
    }
}
