# Changelog

All notable changes to this project will be documented in this file, in reverse chronological order by release.

## 4.3.0
### Updated
- Updated WAYPOINT_SEQUENCE_ENDPOINT to use hereapi v8 findSequence2

## 4.2.0
### Added
- Add matrix routing functionality to find road-miles distance between points

## 4.1.2.
### Added
- Add drive time field to waypoint object
- Add getTotalServiceTime getter on waypoint object

### Updated
- Store distance and drive time when using here.com optimizer

## 4.1.1.
### Added
- Adding PHP8.1 support

## 4.0.0.
### Added
- Here.com support
- Rate limit support

## 1.0.0.
### Added
- Adding a Route4Me provider implementation
- Adding an ORTools python library implementation
- Adding a repo for route optimization
