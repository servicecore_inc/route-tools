<?php

namespace ServiceCore\RouteTools\Data;

use DateTime;

class Waypoint implements OptimizableInterface
{
    private int       $id;
    private string    $name;
    private ?float    $latitude;
    private ?float    $longitude;
    private ?int      $serviceTime;
    private ?DateTime $accessHoursStart;
    private ?DateTime $accessHoursEnd;

    public function __construct(
        int       $id,
        string    $name,
        ?float    $latitude,
        ?float    $longitude,
        ?int      $serviceTime,
        ?DateTime $accessHoursStart,
        ?DateTime $accessHoursEnd
    ) {
        $this->id               = $id;
        $this->name             = $name;
        $this->latitude         = $latitude;
        $this->longitude        = $longitude;
        $this->serviceTime      = $serviceTime;
        $this->accessHoursStart = $accessHoursStart;
        $this->accessHoursEnd   = $accessHoursEnd;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getLatitude(): ?float
    {
        return $this->latitude;
    }

    public function getLongitude(): ?float
    {
        return $this->longitude;
    }

    public function getServiceTime(): ?int
    {
        return $this->serviceTime;
    }

    public function getAccessHoursStart(): ?DateTime
    {
        return $this->accessHoursStart;
    }

    public function getAccessHoursEnd(): ?DateTime
    {
        return $this->accessHoursEnd;
    }
}
