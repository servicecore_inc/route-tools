<?php

namespace ServiceCore\RouteTools\Data;

use DateTime;

interface OptimizableInterface
{
    public function getId(): int;

    public function getName(): string;

    public function getLatitude(): ?float;

    public function getLongitude(): ?float;

    /** Service time is in minutes */
    public function getServiceTime(): ?int;

    public function getAccessHoursStart(): ?DateTime;

    public function getAccessHoursEnd(): ?DateTime;
}
