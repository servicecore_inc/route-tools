<?php

namespace ServiceCore\RouteTools\Collection;

use Countable;
use DateTime;
use InvalidArgumentException;
use ServiceCore\RouteTools\Data\OptimizableInterface;

class Waypoint implements Countable
{
    /**
     * @var OptimizableInterface[]
     */
    private array     $waypoints    = [];
    private bool      $hasLockedEnd = false;
    private int       $distance     = 0;
    private int       $driveTime    = 0;
    private int       $authorityId  = 0;
    private ?DateTime $departure    = null;

    public function __construct(array $waypoints = [])
    {
        foreach ($waypoints as $waypoint) {
            if (!$waypoint instanceof OptimizableInterface) {
                throw new InvalidArgumentException('All Waypoints must implement ' . OptimizableInterface::class);
            }

            $this->waypoints[$waypoint->getId()] = $waypoint;
        }
    }

    /**
     * @param string|int $key
     * @param OptimizableInterface $waypoint
     *
     * @return Waypoint
     */
    public function add($key, OptimizableInterface $waypoint): self
    {
        $this->waypoints[$key] = $waypoint;

        return $this;
    }

    /**
     * @param string|int|null $key
     *
     * @return OptimizableInterface|OptimizableInterface[]
     */
    public function get($key = null)
    {
        if ($key) {
            if (!\array_key_exists($key, $this->waypoints)) {
                throw new InvalidArgumentException('Could not find a location with key: ' . $key);
            }

            return $this->waypoints[$key];
        }

        return $this->waypoints;
    }

    public function getKeys(): array
    {
        return \array_keys($this->waypoints);
    }

    public function reset(): self
    {
        $this->waypoints = [];

        return $this;
    }

    public function count(): int
    {
        return \count($this->waypoints);
    }

    public function first(): OptimizableInterface
    {
        return \reset($this->waypoints);
    }

    public function last(): OptimizableInterface
    {
        return \end($this->waypoints);
    }

    public function slice(int $offset, ?int $length = null): array
    {
        return \array_slice($this->waypoints, $offset, $length, true);
    }

    public function hasLockedEnd(): bool
    {
        return $this->hasLockedEnd;
    }

    public function setHasLockedEnd(bool $hasLockedEnd): self
    {
        $this->hasLockedEnd = $hasLockedEnd;

        return $this;
    }

    public function getDistance(): int
    {
        return $this->distance;
    }

    public function setDistance(int $distance): Waypoint
    {
        $this->distance = $distance;

        return $this;
    }

    public function getDriveTime(): int
    {
        return $this->driveTime;
    }

    public function setDriveTime(int $driveTime): Waypoint
    {
        $this->driveTime = $driveTime;

        return $this;
    }

    public function getAuthorityId(): int
    {
        return $this->authorityId;
    }

    public function setAuthorityId(int $authorityId): Waypoint
    {
        $this->authorityId = $authorityId;

        return $this;
    }

    public function getDeparture(): ?DateTime
    {
        return $this->departure;
    }

    public function setDeparture(?DateTime $departure): Waypoint
    {
        $this->departure = $departure;

        return $this;
    }

    public function getTotalServiceTime(): int
    {
        $total = 0;

        foreach ($this->waypoints as $waypoint) {
            $total += $waypoint->getServiceTime();
        }

        return $total;
    }
}
