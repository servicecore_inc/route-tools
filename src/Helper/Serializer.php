<?php

namespace ServiceCore\RouteTools\Helper;

use DateTime;
use ServiceCore\RouteTools\Collection\Waypoint as WaypointCollection;
use ServiceCore\RouteTools\Data\OptimizableInterface;
use ServiceCore\RouteTools\Data\Waypoint;

class Serializer
{
    public static function serialize(WaypointCollection $waypoints): array
    {
        return \array_map(static function (OptimizableInterface $waypoint, $key) {
            return [
                'key'              => $key,
                'id'               => $waypoint->getId(),
                'name'             => $waypoint->getName(),
                'latitude'         => $waypoint->getLatitude(),
                'longitude'        => $waypoint->getLongitude(),
                'serviceTime'      => $waypoint->getServiceTime(),
                'accessHoursStart' => $waypoint->getAccessHoursStart() ?
                    $waypoint->getAccessHoursStart()->format(DATE_ATOM) :
                    null,
                'accessHoursEnd'   => $waypoint->getAccessHoursEnd() ?
                    $waypoint->getAccessHoursEnd()->format(DATE_ATOM) :
                    null
            ];
        }, $waypoints->get(), $waypoints->getKeys());
    }

    public static function deserialize(array $waypointsData): WaypointCollection
    {
        $collection = new WaypointCollection();

        foreach ($waypointsData as $item) {
            $waypoint = new Waypoint(
                $item['id'],
                $item['name'],
                $item['latitude'],
                $item['longitude'],
                $item['serviceTime'],
                $item['accessHoursStart'] ?
                    DateTime::createFromFormat(DATE_ATOM, $item['accessHoursStart']) : null,
                $item['accessHoursEnd'] ?
                    DateTime::createFromFormat(DATE_ATOM, $item['accessHoursEnd']) : null
            );

            $collection->add($item['key'], $waypoint);
        }

        return $collection;
    }
}
