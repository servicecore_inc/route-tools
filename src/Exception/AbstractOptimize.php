<?php

namespace ServiceCore\RouteTools\Exception;

use Exception;

abstract class AbstractOptimize extends Exception
{
    public array $errorJson;

    public function __construct(string $message, array $errorJson = [], int $code = 422)
    {
        parent::__construct($message, $code);

        $this->errorJson = $errorJson;
    }
}
