<?php

namespace ServiceCore\RouteTools\Exception;

class MissingApiKey extends AbstractOptimize
{
    public function __construct(string $optimizer)
    {
        parent::__construct(\sprintf('Missing API Key for %s', $optimizer));
    }
}
