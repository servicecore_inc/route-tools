<?php

namespace ServiceCore\RouteTools\Exception;

class RateLimitedException extends AbstractOptimize
{
    public function __construct(array $errorJson = [])
    {
        parent::__construct('Rate limiting occurred during route guidance.', $errorJson, 429);
    }
}
