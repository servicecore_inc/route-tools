<?php

namespace ServiceCore\RouteTools\Service;

use InvalidArgumentException;
use ServiceCore\RouteTools\Context\GuidanceInterface;
use ServiceCore\RouteTools\Context\MatrixRoutingInterface;
use ServiceCore\RouteTools\Context\OptimizeInterface;

class RouteTools
{
    public const ORTOOLS  = 'ortools';
    public const ROUTE4ME = 'route4me';
    public const HERE     = 'here';

    private array $optimizeProviders;
    private array $guidanceProviders;
    private array $matrixRoutingProviders;

    public function __construct(array $optimizeProviders, array $guidanceProviders, array $matrixRoutingProviders)
    {
        foreach ($optimizeProviders as $optimizer) {
            if (!$optimizer instanceof OptimizeInterface) {
                throw new InvalidArgumentException(
                    'The \'' . \get_class($optimizer) . '\' context must implement \'' . OptimizeInterface::class . '\''
                );
            }
        }

        foreach ($guidanceProviders as $guidance) {
            if (!$guidance instanceof GuidanceInterface) {
                throw new InvalidArgumentException(
                    'The \'' . \get_class($guidance) . '\' context must implement \'' . GuidanceInterface::class . '\''
                );
            }
        }

        foreach ($matrixRoutingProviders as $matrixRouting) {
            if (!$matrixRouting instanceof MatrixRoutingInterface) {
                throw new InvalidArgumentException(
                    \sprintf(
                        "The '%s' context must implement '%s'",
                        \get_class($matrixRouting),
                        MatrixRoutingInterface::class
                    )
                );
            }
        }

        $this->optimizeProviders      = $optimizeProviders;
        $this->guidanceProviders      = $guidanceProviders;
        $this->matrixRoutingProviders = $matrixRoutingProviders;
    }

    public function getAvailableOptimizers(): array
    {
        return \array_keys($this->optimizeProviders);
    }

    public function getAvailableGuidanceProviders(): array
    {
        return \array_keys($this->guidanceProviders);
    }

    public function getAvailableMatrixRoutingProviders(): array
    {
        return \array_keys($this->matrixRoutingProviders);
    }

    public function getOptimizer(string $optimizer): OptimizeInterface
    {
        if (\array_key_exists($optimizer, $this->optimizeProviders)) {
            return $this->optimizeProviders[$optimizer];
        }

        throw new InvalidArgumentException(
            'Invalid optimizer selected. Available optimizers: '
            . \implode(', ', $this->getAvailableOptimizers())
        );
    }

    public function getGuidanceProvider(string $guidance): GuidanceInterface
    {
        if (\array_key_exists($guidance, $this->guidanceProviders)) {
            return $this->guidanceProviders[$guidance];
        }

        throw new InvalidArgumentException(
            'Invalid optimizer selected. Available optimizers: '
            . \implode(', ', $this->getAvailableGuidanceProviders())
        );
    }

    public function getMatrixRoutingProvider(string $matrixRouting): MatrixRoutingInterface
    {
        if (\array_key_exists($matrixRouting, $this->matrixRoutingProviders)) {
            return $this->matrixRoutingProviders[$matrixRouting];
        }

        throw new InvalidArgumentException(
            'Invalid matrix routing selected. Available matrix routing providers: '
            . \implode(', ', $this->getAvailableMatrixRoutingProviders())
        );
    }
}
