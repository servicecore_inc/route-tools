<?php

namespace ServiceCore\RouteTools\Service\Factory;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use ServiceCore\RouteTools\Context\GuidanceHere;
use ServiceCore\RouteTools\Context\MatrixRoutingHere;
use ServiceCore\RouteTools\Context\OptimizeHere;
use ServiceCore\RouteTools\Context\OptimizeORTools;
use ServiceCore\RouteTools\Context\OptimizeRoute4Me;
use ServiceCore\RouteTools\Service\RouteTools as RouteToolsService;

class RouteTools implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): RouteToolsService
    {
        $optimizeProviders = [
            RouteToolsService::ORTOOLS  => new OptimizeORTools(),
            RouteToolsService::ROUTE4ME => $container->get(OptimizeRoute4Me::class),
            RouteToolsService::HERE     => $container->get(OptimizeHere::class)
        ];

        $guidanceProviders = [
            RouteToolsService::HERE => $container->get(GuidanceHere::class)
        ];

        $matrixRoutingProviders = [
            RouteToolsService::HERE => $container->get(MatrixRoutingHere::class)
        ];

        return new RouteToolsService($optimizeProviders, $guidanceProviders, $matrixRoutingProviders);
    }
}
