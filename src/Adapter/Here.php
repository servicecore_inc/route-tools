<?php

namespace ServiceCore\RouteTools\Adapter;

use DateTime;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use JsonException;
use ServiceCore\RouteTools\Collection\Locations;
use ServiceCore\RouteTools\Collection\Waypoint as WaypointCollection;
use ServiceCore\RouteTools\Data\Location;
use ServiceCore\RouteTools\Data\OptimizableInterface;
use ServiceCore\RouteTools\Exception\InvalidArgumentException;
use ServiceCore\RouteTools\Exception\MissingApiKey;
use ServiceCore\RouteTools\Exception\OptimizationFailed;
use ServiceCore\RouteTools\Exception\RateLimitedException;

class Here
{
    private const WAYPOINT_SEQUENCE_ENDPOINT = 'https://wps.hereapi.com/v8/findsequence2';
    private const ROUTING_ENDPOINT           = 'https://router.hereapi.com/v8/routes';
    private const MATRIX_ROUTING_ENDPOINT    = 'https://matrix.router.hereapi.com/v8/matrix';

    private ?string $apiKey;
    private Client  $client;

    public function __construct(?string $apiKey = null)
    {
        $this->apiKey = $apiKey;
        $this->client = new Client();
    }

    public function optimizeSequence(WaypointCollection $waypoints): array
    {
        if ($this->apiKey === null) {
            throw new MissingApiKey('here.com');
        }

        if ($waypoints->count() < 3) {
            throw new InvalidArgumentException('You must provide at least 3 locations.');
        }

        $count       = 1;
        $queryParams = [
            'apiKey' => $this->apiKey,
            'mode'   => 'fastest;car'
        ];

        if ($waypoints->count() <= 50) {
            $queryParams['mode'] .= ';traffic:enabled';
        }

        if (!$departure = $waypoints->getDeparture()) {
            throw new InvalidArgumentException('You must provide departure time.');
        }

        $queryParams['departure'] = $departure->format(DATE_RFC3339);

        foreach ($waypoints->get() as $key => $waypoint) {
            $timeParams = $this->parseTimeConstraints($waypoint, $departure);

            if ($count === 1) {
                $queryParams['start'] = $this->stringifyLocationWithKey($key, $waypoint);
            } elseif ($count === $waypoints->count() && $waypoints->hasLockedEnd()) {
                $queryParams['end'] = $this->stringifyLocationWithKey($key, $waypoint) . $timeParams;
            } else {
                $queryParams['destination' . ($count - 1)] = $this->stringifyLocationWithKey($key, $waypoint) .
                                                             $timeParams;
            }

            $count++;
        }

        try {
            $response        = $this->client->get(self::WAYPOINT_SEQUENCE_ENDPOINT . '?' . http_build_query($queryParams));
            $decodedResponse = \json_decode($response->getBody()->getContents(), true, 512, \JSON_THROW_ON_ERROR);

            if (\array_key_exists('warnings', $decodedResponse) && $warnings = $decodedResponse['warnings']) {
                $invalidConstraints['warnings'] = [];

                if (\array_key_exists('outOfSequenceWaypoints', $warnings) &&
                    \is_array($warnings['outOfSequenceWaypoints'])) {
                    foreach ($warnings['outOfSequenceWaypoints'] as $waypoint) {
                        $invalidConstraints['warnings'][] = $waypoints->get($waypoint['id'])->getName();
                    }
                }

                throw new OptimizationFailed('The route contained invalid constraints.', $invalidConstraints);
            }

            return $decodedResponse;
        } catch (ClientException|JsonException $e) {
            if ($e instanceof ClientException) {
                $errorJson = $this->parseErrorRequestJson($e);
            }

            throw new OptimizationFailed('There was a problem optimizing the route.', $errorJson ?? []);
        }
    }

    public function guidanceSequence(WaypointCollection $waypoints): array
    {
        if ($this->apiKey === null) {
            throw new MissingApiKey('here.com');
        }

        if ($waypoints->count() < 2) {
            throw new InvalidArgumentException('You must provide at least 2 destinations');
        }

        $count       = 0;
        $queryString = \sprintf(
            '?apiKey=%s&transportMode=%s&return=%s&units=%s',
            $this->apiKey,
            'car',
            'polyline,travelSummary',
            'imperial'
        );

        foreach ($waypoints->get() as $key => $waypoint) {
            $waypointString = $this->stringifyLocation($waypoint);

            if ($count === 0) {
                $queryString .= \sprintf('&origin=%s', $waypointString);
            } elseif ($count === $waypoints->count() - 1) {
                $queryString .= \sprintf('&destination=%s', $waypointString);
            } else {
                $queryString .= \sprintf('&via=%s', $waypointString);
            }

            $count++;
        }

        try {
            $response = $this->client->get(self::ROUTING_ENDPOINT . $queryString);

            return \json_decode($response->getBody()->getContents(), true, 512, \JSON_THROW_ON_ERROR);
        } catch (ClientException $e) {
            if ($e->getCode() === 429) {
                throw new RateLimitedException();
            }

            $errorJson = $this->parseErrorRequestJson($e);

            throw new OptimizationFailed('There was a problem producing route guidance.', $errorJson ?? []);
        } catch (JsonException $e) {
            throw new OptimizationFailed('Invalid JSON in route guidance response.', []);
        }
    }

    public function findDistanceBetweenPoints(OptimizableInterface $origin, WaypointCollection $destinations): array
    {
        if ($this->apiKey === null) {
            throw new MissingApiKey('here.com');
        }

        if ($destinations->count() < 1) {
            throw new InvalidArgumentException('You must provide at least 1 destination');
        }

        $body = [
            'origins'          => [
                [
                    'lat' => $origin->getLatitude(),
                    'lng' => $origin->getLongitude()
                ]
            ],
            'destinations'     => \array_values(\array_map(
                static fn(OptimizableInterface $location) => [
                    'lat' => $location->getLatitude(),
                    'lng' => $location->getLongitude()
                ],
                $destinations->get()
            )),
            'regionDefinition' => [
                'type' => 'autoCircle'
            ],
            'matrixAttributes' => [
                'distances'
            ]
        ];

        $queryString = \sprintf(
            '?async=false&apiKey=%s',
            $this->apiKey
        );

        try {
            $response = $this->client->post(self::MATRIX_ROUTING_ENDPOINT . $queryString, [
                'body' => \json_encode($body, JSON_THROW_ON_ERROR)
            ]);

            return \json_decode($response->getBody()->getContents(), true, 512, \JSON_THROW_ON_ERROR);
        } catch (ClientException $e) {
            if ($e->getCode() === 429) {
                throw new RateLimitedException();
            }

            $errorJson = $this->parseErrorRequestJson($e);

            throw new OptimizationFailed('There was a problem producing matrix routing.', $errorJson);
        } catch (JsonException $e) {
            throw new OptimizationFailed('Invalid JSON in matrix routing response.', []);
        }
    }

    private function stringifyLocationWithKey(string $key, OptimizableInterface $waypoint): string
    {
        return \sprintf('%s;%s', $key, $this->stringifyLocation($waypoint));
    }

    private function stringifyLocation(OptimizableInterface $waypoint): string
    {
        if ($waypoint->getLatitude() === null || $waypoint->getLongitude() === null) {
            throw new InvalidArgumentException(
                \sprintf(
                    'Invalid lat/long for waypoint with ID %s',
                    $waypoint->getId()
                )
            );
        }

        return \sprintf('%s,%s', $waypoint->getLatitude(), $waypoint->getLongitude());
    }

    private function parseTimeConstraints(OptimizableInterface $waypoint, DateTime $departure): string
    {
        $timeParams = '';

        // Work around a bug where jobs are created/materialized with startsAt date set to createdAt date
        $dayOfWeek = \strtolower(\substr($departure->format('D'), 0, 2));
        $year      = $departure->format('Y');
        $month     = $departure->format('m');
        $day       = $departure->format('d');

        if ($accessHoursStart = $waypoint->getAccessHoursStart()) {
            $accessHoursStart->setDate((int)$year, (int)$month, (int)$day);
        }

        if ($accessHoursEnd = $waypoint->getAccessHoursEnd()) {
            $accessHoursEnd->setDate((int)$year, (int)$month, (int)$day);
        }

        // Set time constraints with shortened day of week prepended to access hours
        if ($accessHoursStart && $accessHoursEnd) {
            $timeParams .= \sprintf(';acc:%1$s' . $accessHoursStart->format('H:i:sP') .
                                    '|%1$s' . $accessHoursEnd->format('H:i:sP'), $dayOfWeek);
        }

        // If only startsAt is provided, treat it as appointment time
        if ($accessHoursStart && !$accessHoursEnd) {
            $timeParams .= ';at:' . $accessHoursStart->format(DATE_ATOM);
        }

        // HERE accepts service time in seconds (we store in minutes)
        if ($waypoint->getServiceTime()) {
            $timeParams .= ';st:' . $waypoint->getServiceTime() * 60;
        } else {
            $timeParams .= ';st:0';
        }

        return $timeParams;
    }

    private function parseErrorRequestJson(ClientException $e): array
    {
        $errorResponse = [];

        if (!$e->getResponse()) {
            return $errorResponse;
        }

        $body        = $e->getResponse()->getBody()->getContents();
        $jsonPattern = '/{(?:[^{}]|(?R))*}/x';

        if (\preg_match($jsonPattern, $body, $matches) === 1) {
            $matchedMessage = $matches[0] ?? null;

            if ($matchedMessage !== null) {
                try {
                    $errorResponse = \json_decode($matchedMessage, true, 512, JSON_THROW_ON_ERROR);
                } catch (JsonException $e) {
                    return $errorResponse;
                }

            }
        }

        return $errorResponse;
    }
}
