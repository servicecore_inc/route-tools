<?php

namespace ServiceCore\RouteTools\Context;

use InvalidArgumentException;
use ServiceCore\RouteTools\Collection\Waypoint as WaypointCollection;
use ServiceCore\RouteTools\Data\OptimizableInterface;
use ServiceCore\RouteTools\Exception\OptimizationFailed;

class OptimizeORTools implements OptimizeInterface
{
    public function __invoke(WaypointCollection $waypoints): WaypointCollection
    {
        $locationArg = $this->convertCollectionToArray($waypoints);

        if (!$arguments = \json_encode(['locations' => $locationArg])) {
            throw new InvalidArgumentException('Could not JSON encode `locations`');
        }

        $command = 'python3 ' . \dirname(__DIR__) . '/../bin/optimize_distance.py'
            . ' --json ' . '\'' . $arguments . '\'';

        \exec("{$command}", $response, $exitCode);

        if ($exitCode === 1) {
            throw new OptimizationFailed('ORTools has encountered an error attempting to optimize.');
        }

        if (!$result = \json_decode($response[0], true)) {
            throw new OptimizationFailed('ORTools has encountered an error trying to decode json results.');
        }

        return $this->convertArrayToCollection($result['body'], $waypoints);
    }

    private function convertCollectionToArray(WaypointCollection $locations): array
    {
        $result = [];

        /** @var OptimizableInterface $location */
        foreach ($locations->get() as $key => $location) {
            $result[][$key] = [
                (float)$location->getLatitude(),
                (float)$location->getLongitude()
            ];
        }

        return $result;
    }

    private function convertArrayToCollection(array $results, WaypointCollection $locations): WaypointCollection
    {
        $sortedLocations = new WaypointCollection();

        foreach ($results['optimalRoute'] as $location) {
            $sortedLocations->add($location, $locations->get($location));
        }

        $sortedLocations->setDistance($results['totalDistance']);

        return $sortedLocations;
    }
}
