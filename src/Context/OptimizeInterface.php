<?php

namespace ServiceCore\RouteTools\Context;

use ServiceCore\RouteTools\Collection\Waypoint as WaypointCollection;

interface OptimizeInterface
{
    public function __invoke(WaypointCollection $waypoints): WaypointCollection;
}
