<?php

namespace ServiceCore\RouteTools\Context\Factory;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use ServiceCore\RouteTools\Context\MatrixRoutingHere as MatrixRoutingHereContext;
use ServiceCore\RouteTools\Service\RouteTools as RouteToolsService;

class MatrixRoutingHere implements FactoryInterface
{
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        array $options = null
    ): MatrixRoutingHereContext {
        $config = $container->get('config')['route-tools'];

        return new MatrixRoutingHereContext($config['providers'][RouteToolsService::HERE]['apiKey']);
    }
}
