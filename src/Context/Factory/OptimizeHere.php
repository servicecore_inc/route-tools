<?php

namespace ServiceCore\RouteTools\Context\Factory;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use ServiceCore\RouteTools\Context\OptimizeHere as OptimizeHereContext;
use ServiceCore\RouteTools\Service\RouteTools as RouteToolsService;

class OptimizeHere implements FactoryInterface
{
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        array $options = null
    ): OptimizeHereContext {
        $config = $container->get('config')['route-tools'];

        return new OptimizeHereContext($config['providers'][RouteToolsService::HERE]['apiKey']);
    }
}
