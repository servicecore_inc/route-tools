<?php

namespace ServiceCore\RouteTools\Context\Factory;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use ServiceCore\RouteTools\Context\GuidanceHere as GuidanceHereContext;
use ServiceCore\RouteTools\Service\RouteTools as RouteToolsService;

class GuidanceHere implements FactoryInterface
{
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        array $options = null
    ): GuidanceHereContext {
        $config = $container->get('config')['route-tools'];

        return new GuidanceHereContext($config['providers'][RouteToolsService::HERE]['apiKey']);
    }
}
