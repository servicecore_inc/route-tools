<?php

namespace ServiceCore\RouteTools\Context\Factory;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use ServiceCore\RouteTools\Context\OptimizeRoute4Me as OptimizeRoute4MeContext;
use ServiceCore\RouteTools\Service\RouteTools as RouteToolsService;

class OptimizeRoute4Me implements FactoryInterface
{
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        array $options = null
    ): OptimizeRoute4MeContext {
        $config = $container->get('config')['route-tools'];

        return new OptimizeRoute4MeContext($config['providers'][RouteToolsService::ROUTE4ME]['apiKey']);
    }
}
