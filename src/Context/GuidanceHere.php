<?php

namespace ServiceCore\RouteTools\Context;

use ServiceCore\RouteTools\Adapter\Here as HereAdapter;
use ServiceCore\RouteTools\Collection\Waypoint as WaypointCollection;

class GuidanceHere implements GuidanceInterface
{
    /**
     * @var HereAdapter
     */
    private $adapter;

    public function __construct(?string $apiKey = null)
    {
        $this->adapter = new HereAdapter($apiKey);
    }

    public function __invoke(WaypointCollection $locations): array
    {
        return $this->adapter->guidanceSequence($locations);
    }
}
