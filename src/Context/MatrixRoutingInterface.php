<?php

namespace ServiceCore\RouteTools\Context;

use ServiceCore\RouteTools\Collection\Waypoint as WaypointCollection;
use ServiceCore\RouteTools\Data\OptimizableInterface;

interface MatrixRoutingInterface
{
    public function __invoke(OptimizableInterface $origin, WaypointCollection $destinations): array;
}
