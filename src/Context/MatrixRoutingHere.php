<?php

namespace ServiceCore\RouteTools\Context;

use ServiceCore\RouteTools\Adapter\Here as HereAdapter;
use ServiceCore\RouteTools\Collection\Waypoint as WaypointCollection;
use ServiceCore\RouteTools\Data\OptimizableInterface;

class MatrixRoutingHere implements MatrixRoutingInterface
{
    private HereAdapter $adapter;

    public function __construct(?string $apiKey = null)
    {
        $this->adapter = new HereAdapter($apiKey);
    }

    public function __invoke(OptimizableInterface $origin, WaypointCollection $destinations): array
    {
        return $this->adapter->findDistanceBetweenPoints($origin, $destinations);
    }
}
