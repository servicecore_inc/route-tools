<?php

namespace ServiceCore\RouteTools\Context;

use Route4Me\Address;
use Route4Me\Enum\AlgorithmType;
use Route4Me\Enum\DeviceType;
use Route4Me\Enum\DistanceUnit;
use Route4Me\Enum\OptimizationType;
use Route4Me\Enum\TravelMode;
use Route4Me\Exception\ApiError;
use Route4Me\OptimizationProblem;
use Route4Me\OptimizationProblemParams;
use Route4Me\Route;
use Route4Me\Route4Me;
use Route4Me\RouteParameters;
use ServiceCore\RouteTools\Collection\Waypoint as WaypointCollection;
use ServiceCore\RouteTools\Exception\MissingApiKey;
use ServiceCore\RouteTools\Exception\OptimizationFailed;

class OptimizeRoute4Me implements OptimizeInterface
{
    /** @var null|string */
    private $apiKey;

    public function __construct(?string $apiKey = null)
    {
        $this->apiKey = $apiKey;
    }

    public function __invoke(WaypointCollection $waypoints): WaypointCollection
    {
        if ($this->apiKey === null) {
            throw new MissingApiKey('Route4Me');
        }

        Route4Me::setApiKey($this->apiKey);

        $addresses = [];

        foreach ($waypoints->get() as $key => $location) {
            $addresses[] = Address::fromArray([
                'lat'          => $location->getLatitude(),
                'lng'          => $location->getLongitude(),
                'reference_no' => $key,
                'is_depot'     => $key === 'home',
                'account_no'   => $waypoints->getAuthorityId()
            ]);
        }

        $parameters = RouteParameters::fromArray([
            'algorithm_type'          => AlgorithmType::TSP,
            'distance_unit'           => DistanceUnit::MILES,
            'device_type'             => DeviceType::WEB,
            'optimize'                => OptimizationType::DISTANCE,
            'travel_mode'             => TravelMode::DRIVING,
            'route_max_duration'      => 86400,
            'vehicle_capacity'        => 1,
            'vehicle_max_distance_mi' => 10000
        ]);

        $optimizationParams = new OptimizationProblemParams();
        $optimizationParams->setAddresses($addresses);
        $optimizationParams->setParameters($parameters);

        try {
            $problem = OptimizationProblem::optimize($optimizationParams);
        } catch (ApiError $e) {
            $message = $e->getMessage();

            if ($message === 'Something wrong') {
                $message = 'Route4Me has encountered an unknown error or timed out.';
            } elseif ($message === 'Wrong API key') {
                $message = 'Route4Me has been incorrectly configured.  Please notify a ServiceCore representative.';
            }

            throw new OptimizationFailed($message);
        }

        if ($problem->user_errors !== []) {
            throw new OptimizationFailed(
                'Provider Route4Me encountered errors: ' . \implode(', ', $problem->user_errors)
            );
        }

        /** @var Route[] $routes */
        $routes = $problem->getRoutes();

        return $this->parseRouteIntoLocations(\array_shift($routes), $waypoints);
    }

    private function parseRouteIntoLocations(Route $route, WaypointCollection $locations): WaypointCollection
    {
        $orderedLocations = new WaypointCollection();

        /** @var Address $address */
        foreach ($route->addresses as $address) {
            $orderedLocations->add($address->reference_no, $locations->get($address->reference_no));
        }

        $orderedLocations->setDistance(\round($route->trip_distance));

        return $orderedLocations;
    }
}
