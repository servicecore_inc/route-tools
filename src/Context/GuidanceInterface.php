<?php

namespace ServiceCore\RouteTools\Context;

use ServiceCore\RouteTools\Collection\Waypoint as WaypointCollection;

interface GuidanceInterface
{
    public function __invoke(WaypointCollection $locations): array;
}
