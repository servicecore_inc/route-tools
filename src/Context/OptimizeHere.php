<?php

namespace ServiceCore\RouteTools\Context;

use ServiceCore\RouteTools\Adapter\Here as HereAdapter;
use ServiceCore\RouteTools\Collection\Waypoint as WaypointCollection;

class OptimizeHere implements OptimizeInterface
{
    /**
     * @var HereAdapter
     */
    private $adapter;

    public function __construct(?string $apiKey = null)
    {
        $this->adapter = new HereAdapter($apiKey);
    }

    public function __invoke(WaypointCollection $waypoints): WaypointCollection
    {
        $response         = $this->adapter->optimizeSequence($waypoints);
        $orderedSequence  = $response['results'][0]['waypoints'];

        $orderedWaypoints = new WaypointCollection();

        foreach ($orderedSequence as $waypoint) {
            $orderedWaypoints->add($waypoint['id'], $waypoints->get($waypoint['id']));
        }

        $orderedWaypoints->setDistance($response['results'][0]['distance']);      // meters
        $orderedWaypoints->setDriveTime($response['results'][0]['time']);         // seconds

        return $orderedWaypoints;
    }
}
