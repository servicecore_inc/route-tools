import argparse
import collections
import json
import math

from ortools.constraint_solver import pywrapcp
from ortools.constraint_solver import routing_enums_pb2

# This must be updated in order to run on OR-Tools v7.0
# see https://developers.google.com/optimization/support/release_notes#index-manager
class Optimize(object):
    def __init__(self, locations):
        self.locations = locations

    def run(self):
        body = self.locations

        try:
            locations = body['locations']
        except ValueError:

            return self.get_error_message('`locations` not found in body')

        location_names = []
        location_coordinates = []

        if locations and isinstance(locations, collections.Iterable):
            for location in locations:
                for l in location:
                    location_names.append(l)
                    location_coordinates.append(location[l])
            depot = 0
            num_routes = 1

            routing = pywrapcp.RoutingModel(len(location_coordinates), num_routes, depot)
            search_parameters = pywrapcp.RoutingModel.DefaultSearchParameters()

            # Setting first solution heuristic: the
            # method for finding a first solution to the problem.
            search_parameters.first_solution_strategy = (
                routing_enums_pb2.FirstSolutionStrategy.PATH_CHEAPEST_ARC
            )

            # The 'PATH_CHEAPEST_ARC' method does the following:
            # Starting from a route "start" node, connect it to the node which produces the
            # cheapest route segment, then extend the route by iterating on the last
            # node added to the route.
            distance = Distance(location_coordinates)
            dist_callback = distance.get_callback
            routing.SetArcCostEvaluatorOfAllVehicles(dist_callback)

            body = {
                "optimalRoute": []
            }

            return_data = {
                "body": ""
            }

            # Solve, displays a solution if any.
            assignment = routing.SolveWithParameters(search_parameters)
            if assignment:
                body['totalDistance'] = str(assignment.ObjectiveValue())
                # Only one route here; otherwise iterate from 0 to routing.vehicles() - 1
                route_number = 0
                index = routing.Start(route_number)
                while not routing.IsEnd(index):
                    body['optimalRoute'].append(location_names[routing.IndexToNode(index)])
                    index = assignment.Value(routing.NextVar(index))
                body['optimalRoute'].append(location_names[routing.IndexToNode(index)])

                return_data['body'] = body

                return return_data
            else:
                return self.get_error_message('Could not find a solution.')
        else:
            return self.get_error_message('Could not parse `locations` from `body`.')

    def get_error_message(self, message=None):
        if message is None:
            message = "An error occurred."

        body = {
            "Error": message
        }

        return_data = {
            "body": ""
        }

        return_data['body'] = body

        return return_data


class Distance(object):
    def __init__(self, locations):
        # Mean radius of Earth in miles
        self.radius_of_earth = 3959

        # Convert latitude and longitude to spherical coordinates in radians.
        self.degrees_to_radians = math.pi / 180.0

        # A dict of latitude/longitudes
        self.locations = locations

    def get_callback(self, from_arg, to_arg):
        x1 = self.locations[from_arg][0]
        y1 = self.locations[from_arg][1]
        x2 = self.locations[to_arg][0]
        y2 = self.locations[to_arg][1]

        return self.get_distance_between_coordinates(x1, y1, x2, y2)

    def get_distance_between_coordinates(self, lat1, long1, lat2, long2):
        phi1 = lat1 * self.degrees_to_radians
        phi2 = lat2 * self.degrees_to_radians

        lambda1 = long1 * self.degrees_to_radians
        lambda2 = long2 * self.degrees_to_radians

        dphi = phi2 - phi1
        dlambda = lambda2 - lambda1

        a = self.get_haversine(dphi) + math.cos(phi1) * math.cos(phi2) * self.get_haversine(dlambda)
        c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))

        return self.radius_of_earth * c

    @staticmethod
    def get_manhattan_distance_between_coordinates(lat1, long1, lat2, long2):
        return abs(lat1 - lat2) + abs(long1 - long2)

    @staticmethod
    def get_haversine(angle):
        return math.sin(angle / 2) ** 2


parser = argparse.ArgumentParser(description='Optimize a route')
parser.add_argument('--json', type=str, help='JSON string of locations to optimize')

args = parser.parse_args()

optimize = Optimize(json.loads(args.json))

print(json.dumps(optimize.run()))
